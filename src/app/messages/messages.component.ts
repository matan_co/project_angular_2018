import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import { Router } from "@angular/router";
import * as JWT from 'jwt-decode';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  messages;
  messagesKeys =[];  
  token;
  user_id;
  

  optimisticAdd(message){	 
	  var newKey =  this.messagesKeys[this.messagesKeys.length-1] +1;
	  var newMessageObject ={};
	  newMessageObject['body'] = message;
    newMessageObject['created_at'] = new Date();
    newMessageObject['liked'] = false;
    newMessageObject['count'] = 0;
	  this.messages[newKey] = newMessageObject;
	  this.messagesKeys = Object.keys(this.messages);
  }
  
  pasemisticAdd(){
	  this.service.getMessages().subscribe(response=>{      
	      this.messages = response.json();
	      this.messagesKeys = Object.keys(this.messages);  
        let count = 0;      
        let key = '';
        this.messagesKeys.forEach(element => {
            this.messages[element].liked = false;          
            this.service.db_likes().subscribe(actions => {              
              actions.forEach(action => {                   
                key = action.key;              
                action = action.payload.val();                                                          
                if(element == action.message_id){
                  if(action.user_id == this.user_id){
                    this.messages[element].liked = true;  
                    this.messages[element].ident = key;
                  }
                  count++;                                
                }                                        
              });
              this.messages[element].count = count;              
              count = 0;            
            });  
        });   
	  });    
  }  

  like(key){    
    this.service.setLike(key, this.user_id);
  }

  dislike(key, id){
    this.service.deleteLike(key);
    this.messages[id].liked = false;  
  }

  constructor(private service:MessagesService, private router:Router, private spinnerService: Ng4LoadingSpinnerService) {   
  	this.token = localStorage.getItem('token');
    this.user_id = localStorage.getItem('user_id');    
    this.spinnerService.show();
  	
    service.getMessages().subscribe(response=>{      
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);      
      let count = 0;      
      let key = '';
      this.messagesKeys.forEach(element => {
          this.messages[element].liked = false;          
          this.service.db_likes().subscribe(actions => {              
            actions.forEach(action => {                   
              key = action.key;              
              action = action.payload.val();                                                          
              if(element == action.message_id){
                if(action.user_id == this.user_id){
                  this.messages[element].liked = true;  
                  this.messages[element].ident = key;
                }
                count++;                                
              }                                        
            });
            this.messages[element].count = count;              
            count = 0;            
          });  
      });
      this.spinnerService.hide();      
  	});
  }

  ngOnInit() {  	
  	if(this.token){
      let expire = {};
      expire  = JWT(this.token)['expire'];      
      let time   = Math.floor(Date.now()/1000);      

      if(expire < time){
        localStorage.removeItem('token');
        this.router.navigate(['/']);      
      }
    }else{
      this.router.navigate(['/']);
    }   	  	  	  
  }

}
