import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import * as JWT from 'jwt-decode';
import { UsersService } from './users.service';
import { MessagesService } from '../messages/messages.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  // Variables
  token;
  user_id;
  users;
  usersKeys;
  messages;
  messagesKeys;
  length;

  constructor(private service:UsersService, private router:Router, private serviceMsg:MessagesService, private spinnerService: Ng4LoadingSpinnerService) {
  	this.token   = localStorage.getItem('token');
    this.user_id = localStorage.getItem('user_id');
        
  	service.getUser().subscribe(response=>{        		
  		this.users = response.json();
  		this.usersKeys = Object.keys(this.users);    
   	});  

    this.spinnerService.show();
    serviceMsg.getMessageByUser(this.user_id).subscribe(response=>{      
      this.messages = response.json();
      this.messagesKeys = Object.keys(this.messages);    
      this.length = this.messagesKeys.length;
      this.spinnerService.hide();
    });
  }

  deleteMessage(key){   
    let index = this.messagesKeys.indexOf(key);
    this.messagesKeys.splice(index,1);
    this.serviceMsg.deleteMessage(key).subscribe(response=> console.log(response));
  }

  ngOnInit() {  	
  	if(this.token){
      let expire = {};
  		expire  = JWT(this.token)['expire'];      
	  	let time 	= Math.floor(Date.now()/1000);      

	  	if(expire < time){
	  		localStorage.removeItem('token');
	  		this.router.navigate(['/']);    	
	  	}
	  }else{
	  	this.router.navigate(['/']);
	  }   	  	  	  
  }

}
