import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class UsersService {
  // Variables
  httpClient:HttpClient;
  http:Http;
  token;
  user_id;
  headers: Headers;

  getUser(){  
      let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }

     return this.http.get(environment.url + '/users/' + this.user_id ,options);
  }

  getUserById(id){  
      let options =  {
        headers:new Headers({
          'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
          'Token': this.token    
        })
      }

     return this.http.get(environment.url + '/users/' + id ,options);
  }

  postUser(data){       
    var headers = new HttpHeaders();    
    headers.append('Content-Type', 'application/form-data');


    return this.httpClient.post(environment.url + '/users/create', data, { headers: headers });
  }

  constructor(httpClient:HttpClient,http:Http) {
  	this.http    = http;
    this.httpClient = httpClient;
  	this.token   = localStorage.getItem('token');
  	this.user_id = localStorage.getItem('user_id');
    this.headers = new Headers();
    this.headers.set('Content-Type', 'application/x-www-form-urlencoded');
  }

}
