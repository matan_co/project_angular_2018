import { Component, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, Validators} from "@angular/forms";
import { OnInit, Output, EventEmitter } from '@angular/core';
import { UsersService } from './../users.service';
import { FormGroup , FormControl } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {  
  
  @ViewChild('fileInput') fileInput;

  // Variables
  user_id;
  token;

  service:UsersService;
  usrform = new FormGroup({
  	  name:new FormControl('',  Validators.required),
      phone:new FormControl('',  Validators.required),
      password:new FormControl('',  Validators.required),
      image:new FormControl('',  Validators.required),
      email:new FormControl('',  Validators.required),
      gender:new FormControl('',  Validators.required),
      dob:new FormControl('',  Validators.required)
  });

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.usrform.get('image').setValue(file);
    }
  }

  private prepareSave(): any {
    let formModel = new FormData();
    formModel.append('name', this.usrform.get('name').value);
    formModel.append('phone', this.usrform.get('phone').value);
    formModel.append('image', this.usrform.get('image').value);
    formModel.append('email', this.usrform.get('email').value);
    formModel.append('password', this.usrform.get('password').value);
    formModel.append('gender', this.usrform.get('gender').value);
    formModel.append('dob', this.usrform.get('dob').value);

    return formModel;
  }

  sendData(){      
    const formModel = this.prepareSave();	  	

  	this.service.postUser(formModel).subscribe(response =>{   		
  		 this.router.navigate(['/login']);
  	});
  }

  constructor(service:UsersService, private router:Router) {
    this.service = service;
    this.user_id = localStorage.getItem('user_id');    
    this.token   = localStorage.getItem('token');
   }

  ngOnInit() {    
    if(this.user_id){
      this.router.navigate(['/users']);
    }              
  }

}
