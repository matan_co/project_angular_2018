import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpParams, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { environment } from '../environments/environment';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UsersComponent } from './users/users.component';
import { LoginService } from './login/login.service';
import { UsersService } from './users/users.service';
import { MessagesService } from './messages/messages.service';
import { MessagesComponent } from './messages/messages.component';
import { CreateMessageComponent } from './messages/create-message/create-message.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ShowUserComponent } from './users/show-user/show-user.component';

//JWT
import * as JWT from 'jwt-decode';



const appRoutes: Routes = [
  { path: ''            	     , component: LoginComponent       },
  { path: 'feed'               , component: MessagesComponent    },
  { path: 'signup'             , component: UsersFormComponent    },
  { path: 'users'              , component: UsersComponent       },
  { path: 'show-user/:id'      , component: ShowUserComponent       },
  { path: 'login'              , component: LoginComponent       },
  { path: '**'                 , component: NotFoundComponent    }  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    UsersComponent,
    MessagesComponent,
    CreateMessageComponent,
    UsersFormComponent,
    NavigationComponent,
    ShowUserComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFontAwesomeModule,
    Ng4LoadingSpinnerModule.forRoot(),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(      
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [LoginService, UsersService, MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
